from flask import Flask, jsonify
import requests
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)


class WeatherById(Resource):

    def __init__(self):
        self.url = "https://samples.openweathermap.org/data/2.5/forecast/hourly?id=524901&appid=b6907d289e10d714a6e88b30761fae22"
        self.weather = {}

    def get(self, id):
        try:
            response = requests.get(url=self.url, timeout=2).json()
            self.weather = {'weather_desc': response["list"][id]["weather"][0]["description"],
                            'temperature': response["list"][id]["main"]["temp"],
                            'wind_speed': response["list"][id]["wind"]["speed"]}
            return self.weather, 201

        except requests.exceptions.Timeout:
            return "Bad request"


class WeatherList(Resource):

    def __init__(self):
        self.url = "https://samples.openweathermap.org/data/2.5/forecast/hourly?id=524901&appid=b6907d289e10d714a6e88b30761fae22"
        self.weather_all = []

    def get(self):
        try:
            response = requests.get(url=self.url, timeout=2).json()
            for i in response['list']:
                data = {'weather_desc': i["weather"][0]["description"],
                        'temperature': i["main"]["temp"],
                        'wind_speed': i["wind"]["speed"]}
                self.weather_all.append(data)
            return self.weather_all, 200

        except requests.exceptions.Timeout:
            return "Bad request"


api.add_resource(WeatherById, '/<int:id>')
api.add_resource(WeatherList, '/')

if __name__ == '__main__':
    app.run(debug=True, port=5001)
