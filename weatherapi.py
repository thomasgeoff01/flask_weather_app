import requests
from flask import Flask, jsonify

app = Flask(__name__)


@app.route('/<int:id>')
def index(id):
    #this is the index file
    url = "https://samples.openweathermap.org/data/2.5/forecast/hourly?id=524901&appid=b6907d289e10d714a6e88b30761fae22"
    response = requests.get(url).json()
    weather = {'weather_desc': response["list"][id]["weather"][0]["description"],
               'temperature': response["list"][id]["main"]["temp"],
               'wind_speed': response["list"][id]["wind"]["speed"]}
    return weather


@app.route('/')
def all_data():
    url = "https://samples.openweathermap.org/data/2.5/forecast/hourly?id=524901&appid=b6907d289e10d714a6e88b30761fae22"
    response = requests.get(url).json()
    lst = []
    for i in response['list']:
        data = {'weather_desc': i["weather"][0]["description"],
                'temperature': i["main"]["temp"],
                'wind_speed': i["wind"]["speed"]}
        lst.append(data)
    return jsonify(lst)


if __name__ == '__main__':
    app.run(debug=True, port=5001)
